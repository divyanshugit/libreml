From 4be8e454625559ac6c59f1f4aef553f1c291d364 Mon Sep 17 00:00:00 2001
From: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Date: Fri, 3 Apr 2020 21:56:02 +0100
Subject: [PATCH] Xfail NaN <-> NaT tests on non-x86 and warn on cast

pd.Series([np.nan]).astype('datetime64[ns]')[0] = pd.NaT on x86
but 1970-01-01 on arm* because float NaN -> int is undefined:
https://github.com/numpy/numpy/issues/8325
https://github.com/pandas-dev/pandas/issues/17792
https://github.com/pandas-dev/pandas/issues/26964

On s390x it's the maximum _positive_ value (2**63-1 ns = year 2262)

Author: Andreas Tille <tille@debian.org>, Graham Inggs <ginggs@debian.org>, Rebecca N. Palmer <rebecca_palmer@zoho.com>
Bug-Debian: https://bugs.debian.org/877754

Gbp-Pq: Name xfail_tests_nonintel_nannat.patch
---
 pandas/core/dtypes/cast.py                      | 8 ++++++++
 pandas/tests/dtypes/cast/test_downcast.py       | 4 ++++
 pandas/tests/frame/test_analytics.py            | 4 ++++
 pandas/tests/indexes/datetimes/test_datetime.py | 4 ++++
 pandas/tests/reductions/test_reductions.py      | 4 ++++
 pandas/tests/series/test_constructors.py        | 5 +++++
 pandas/tests/test_algos.py                      | 5 ++++-
 7 files changed, 33 insertions(+), 1 deletion(-)

diff --git a/pandas/core/dtypes/cast.py b/pandas/core/dtypes/cast.py
index eecb1e567..4c4286c5a 100644
--- a/pandas/core/dtypes/cast.py
+++ b/pandas/core/dtypes/cast.py
@@ -1,6 +1,10 @@
 """ routings for casting """
 
 from datetime import datetime, timedelta
+import warnings
+import platform
+import re
+warn_nannat_platform = "Non-x86 system detected, float -> datetime/timedelta may not handle NaNs correctly - https://bugs.debian.org/877754" if not bool(re.match('i.?86|x86',platform.uname()[4])) else False
 
 import numpy as np
 
@@ -891,6 +895,8 @@ def astype_nansafe(arr, dtype, copy: bool = True, skipna: bool = False):
             f"'{dtype.name}[ns]' instead."
         )
         raise ValueError(msg)
+    if warn_nannat_platform and (is_datetime64_dtype(dtype) or is_timedelta64_dtype(dtype)) and np.issubdtype(arr.dtype, np.floating) and not np.isfinite(arr).all():
+        warnings.warn(warn_nannat_platform)
 
     if copy or is_object_dtype(arr) or is_object_dtype(dtype):
         # Explicit copy, or required since NumPy can't view from / to object.
@@ -1263,6 +1269,8 @@ def maybe_cast_to_datetime(value, dtype, errors: str = "raise"):
                     value = iNaT
             else:
                 value = np.array(value, copy=False)
+                if warn_nannat_platform and np.issubdtype(value.dtype, np.floating) and not np.isfinite(value).all():
+                    warnings.warn(warn_nannat_platform)
 
                 # have a scalar array-like (e.g. NaT)
                 if value.ndim == 0:
diff --git a/pandas/tests/dtypes/cast/test_downcast.py b/pandas/tests/dtypes/cast/test_downcast.py
index d6e6ed302..0bfe74af8 100644
--- a/pandas/tests/dtypes/cast/test_downcast.py
+++ b/pandas/tests/dtypes/cast/test_downcast.py
@@ -7,6 +7,9 @@ from pandas.core.dtypes.cast import maybe_downcast_to_dtype
 
 from pandas import DatetimeIndex, Series, Timestamp
 import pandas._testing as tm
+import platform
+import re
+is_nannat_working=bool(re.match('i.?86|x86',platform.uname()[4]))
 
 
 @pytest.mark.parametrize(
@@ -77,6 +80,7 @@ def test_downcast_conversion_empty(any_real_dtype):
     tm.assert_numpy_array_equal(result, np.array([], dtype=np.int64))
 
 
+@pytest.mark.xfail(condition=not is_nannat_working,reason="https://bugs.debian.org/877754",strict=False)
 @pytest.mark.parametrize("klass", [np.datetime64, np.timedelta64])
 def test_datetime_likes_nan(klass):
     dtype = klass.__name__ + "[ns]"
diff --git a/pandas/tests/frame/test_analytics.py b/pandas/tests/frame/test_analytics.py
index 25b2997eb..ccbeab7fd 100644
--- a/pandas/tests/frame/test_analytics.py
+++ b/pandas/tests/frame/test_analytics.py
@@ -4,6 +4,9 @@ import operator
 
 import numpy as np
 import pytest
+import platform
+import re
+is_nannat_working=bool(re.match('i.?86|x86',platform.uname()[4]))
 
 import pandas.util._test_decorators as td
 
@@ -790,6 +793,7 @@ class TestDataFrameAnalytics:
         expected = pd.Series(result, index=["A", "B"])
         tm.assert_series_equal(result, expected)
 
+    @pytest.mark.xfail(condition=not is_nannat_working,reason="https://bugs.debian.org/877754",strict=False)
     def test_sum_nanops_timedelta(self):
         # prod isn't defined on timedeltas
         idx = ["a", "b", "c"]
diff --git a/pandas/tests/indexes/datetimes/test_datetime.py b/pandas/tests/indexes/datetimes/test_datetime.py
index ca18d6fbe..d80df9e6b 100644
--- a/pandas/tests/indexes/datetimes/test_datetime.py
+++ b/pandas/tests/indexes/datetimes/test_datetime.py
@@ -3,6 +3,9 @@ from datetime import date
 import dateutil
 import numpy as np
 import pytest
+import platform
+import re
+is_nannat_working=bool(re.match('i.?86|x86',platform.uname()[4]))
 
 import pandas as pd
 from pandas import DataFrame, DatetimeIndex, Index, Timestamp, date_range, offsets
@@ -63,6 +66,7 @@ class TestDatetimeIndex:
         idx2 = pd.date_range(end="2000", periods=periods, freq="S")
         assert len(idx2) == periods
 
+    @pytest.mark.xfail(condition=not is_nannat_working,reason="https://bugs.debian.org/877754",strict=False)
     def test_nat(self):
         assert DatetimeIndex([np.nan])[0] is pd.NaT
 
diff --git a/pandas/tests/reductions/test_reductions.py b/pandas/tests/reductions/test_reductions.py
index 7400b0499..bb83214d4 100644
--- a/pandas/tests/reductions/test_reductions.py
+++ b/pandas/tests/reductions/test_reductions.py
@@ -2,6 +2,9 @@ from datetime import datetime, timedelta
 
 import numpy as np
 import pytest
+import platform
+import re
+is_nannat_working=bool(re.match('i.?86|x86',platform.uname()[4]))
 
 import pandas as pd
 from pandas import (
@@ -1142,6 +1145,7 @@ class TestSeriesMode:
         expected = Series(expected2, dtype=object)
         tm.assert_series_equal(result, expected)
 
+    @pytest.mark.xfail(condition=not is_nannat_working,reason="https://bugs.debian.org/877754",strict=False)
     @pytest.mark.parametrize(
         "dropna, expected1, expected2",
         [
diff --git a/pandas/tests/series/test_constructors.py b/pandas/tests/series/test_constructors.py
index c38e5708b..e439f45dc 100644
--- a/pandas/tests/series/test_constructors.py
+++ b/pandas/tests/series/test_constructors.py
@@ -10,6 +10,9 @@ from pandas._libs.tslib import iNaT
 
 from pandas.core.dtypes.common import is_categorical_dtype, is_datetime64tz_dtype
 from pandas.core.dtypes.dtypes import CategoricalDtype
+import platform
+import re
+is_nannat_working=bool(re.match('i.?86|x86',platform.uname()[4]))
 
 import pandas as pd
 from pandas import (
@@ -960,6 +963,7 @@ class TestSeriesConstructors:
 
         tm.assert_series_equal(result, expected)
 
+    @pytest.mark.xfail(condition=not is_nannat_working,reason="https://bugs.debian.org/877754",strict=False)
     @pytest.mark.parametrize("arg", ["2013-01-01 00:00:00", pd.NaT, np.nan, None])
     def test_constructor_with_naive_string_and_datetimetz_dtype(self, arg):
         # GH 17415: With naive string
@@ -1272,6 +1276,7 @@ class TestSeriesConstructors:
         series[2] = val
         assert isna(series[2])
 
+    @pytest.mark.xfail(condition=not is_nannat_working,reason="https://bugs.debian.org/877754",strict=False)
     def test_NaT_cast(self):
         # GH10747
         result = Series([np.nan]).astype("M8[ns]")
diff --git a/pandas/tests/test_algos.py b/pandas/tests/test_algos.py
index 57ee3bedd..182dab1c2 100644
--- a/pandas/tests/test_algos.py
+++ b/pandas/tests/test_algos.py
@@ -35,7 +35,9 @@ from pandas.conftest import BYTES_DTYPES, STRING_DTYPES
 import pandas.core.algorithms as algos
 from pandas.core.arrays import DatetimeArray
 import pandas.core.common as com
-
+import platform
+import re
+is_nannat_working=bool(re.match('i.?86|x86',platform.uname()[4]))
 
 class TestFactorize:
     def test_basic(self):
@@ -1046,6 +1048,7 @@ class TestValueCounts:
             expected = Series([2, 1, 1], index=[5.0, 10.3, np.nan])
             tm.assert_series_equal(result, expected)
 
+    @pytest.mark.xfail(condition=not is_nannat_working,reason="https://bugs.debian.org/877754",strict=False)
     def test_value_counts_normalized(self):
         # GH12558
         s = Series([1, 2, np.nan, np.nan, np.nan])
-- 
2.11.0

